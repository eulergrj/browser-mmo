import express from "express";
import * as http from "http"
import SocketIO from "socket.io"
import State from "./state"
import path from "path"
import Player from "./src/js/classes/Player";
import {loadMobs} from "./src/js/gameData"
import { Socket } from "dgram";
import Config from "../shared/config";
import {getDistance} from "./src/js/utils";

const app = express(),
    server = http.createServer(app),
    io = SocketIO(server);

app.use(express.static(path.join(__dirname, '../../client')));
 
server.listen(3000, () => {
    console.log("Server started on port 3000");
});


loadMobs();



io.on("connection", (socket) => {      
			
	State.sockets[Math.random()] = socket; 		
	
	let player = new Player({
		name: "Player " + Math.floor(Math.random() * 10), 
		sid:socket.id, 
		id:'' + Math.random(), 
		x: 600, 
		y: 2652, 
		z: 0,
		maxHp: 200, 
		hp: 200, 
		outfit: "default",
		dead: false,
		attackSpeed: 1,		
	});		
	State.players.push(player);

	player.connect(socket);
	
	socket.on('disconnect',function(){
		player.disconnect(socket);
		delete State.sockets[socket.id];			
		io.emit("remoteSocketDisconnect", socket.id);			
	});	
    
});



// const filterPlayers = () => {
// 	selfPlayer = 
// }


setInterval(function(){
	// for(var i in State.sockets){
	// 	var socket = State.sockets[i];		
	// }		

	Object.keys(State.sockets).forEach(key => {      	
		let socket  = State.sockets[key];
		let players = State.players;		

		let data = {};

		let self = players.filter(p => p.id == socket.id)[0];
		
		if(players.length > 1){
			players.filter(player => {			
				if(player &&  self && player != self){
					let distance = getDistance(self, player);
										
					if(distance >= 1000){
						players = players.filter(p => p.id != player.id);      
					}
				}							
			})
		}

		data["players"] = players;

		// TODO: filter only mobs visible to the player
		data["mobs"] = State.mobs;						
						
		socket.emit("update", data);
    });    
		
},Config.interval);