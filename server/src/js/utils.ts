import Creature from "../js/classes/Creature";

export const getDistance = (self: Creature, creature: Creature) => {
	return Math.floor(Math.sqrt(Math.pow(self.x - creature.x,2) + Math.pow(self.y - creature.y,2)));
}

export const getDistanceFromPoint = (self: Creature, x: number, y:number) => {
	return Math.floor(Math.sqrt(Math.pow(self.x - x,2) + Math.pow(self.y - y,2)));
}