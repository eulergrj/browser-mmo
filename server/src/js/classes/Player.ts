import Creature from "./Creature";
import State from "../../../state";
import {playerConstructor} from "../../../../shared/interfaces";
import {getDistance} from "../utils"

class Player extends Creature{       
    sid: string;    

    constructor(data: playerConstructor){
        super(data);
        this.sid = data.sid;
    }
    
    connect(socket){
        socket.emit('init', {
            selfId: socket.id,
            players: State.players,            
        });
        
        socket.on("move", data => this.move(data.N, data.E, data.S, data.W))

        socket.on("toggleSprint", () => this.sprint = !this.sprint)

        socket.on("escapeKey", () => {
            this.stopMovement();
            this.removeTarget();
            this.stopFollow();            
        })

        socket.on("moveTo", (x,y) => {                                            
            this.moveTo(x, y);            
        })

        socket.on("stopMovement", () => {   
            !this.movementInProgress && this.stopMovement();            
        })
        
        
        socket.on("targetCreature", (cid: string, attack: boolean) => {                    
            let creatures = [...State.players, ...State.mobs];
            let creature = Object.values(creatures).find(c => c.id == cid);
            let player = Object.values(State.players).find(p => p.sid == socket.id);
            let canTarget = true;

            if(creature.id == player.id) canTarget = false;
            
            if(getDistance(player, creature) >= 600){                
                socket.emit("serverMessage", "He is too far away!");
                canTarget = false;
            }
                             
            if(canTarget){
                if(attack) player.attackCreature(creature);
                else player.targetCreature(creature.id);            
            }
        })

        socket.on("removeTarget", () => {                    
            this.removeTarget();          
        })
    }

    disconnect(socket){       
        setTimeout(()=>{
            State.players = State.players.filter(player => player.sid != socket.id);        
        }, 6000);       
    }


    takeDamage(damage: number, attacker?: Creature){
        super.takeDamage(damage, attacker);
        
        !this.target && this.targetCreature(attacker.id);
    }


    processDeath() : void{        
        super.processDeath();        
        this.removeTarget();
        this.respawn();
    }

    respawn():void{       
        setTimeout(() => {
            this.hp   = this.maxHp;
            this.x    = 1000;
            this.y    = 0;
            this.z    = 0;
            this.dead = false;
        },500);
    };
}

export default Player;