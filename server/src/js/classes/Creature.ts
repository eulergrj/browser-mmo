import State from "../../../state";
import Config from "../../../../shared/config";
import {creatureConstructor} from "../../../../shared/interfaces";
import {getDistance} from "../utils";
import { setInterval } from "timers";


class Creature{           
    // info
    id:                  string;
    name:                string;            
    
    // visual
    sizeW:               number;
    sizeH:               number;
    outfit?:             string;


    //  movement
    x:                   number;
    y:                   number;
    z:                   number;
    isMoving:            boolean;
    direction:           string;
    speed:               number;
    sprint:              boolean;
    speedModifier?:      number;
    movementInProgress?: boolean;
    isFollowing?:        boolean;

    // attacking
    target?:             string;
    attacking?:          string;  
    attackInProggress?:  boolean;
    dead:                boolean;
    recentDeath:         boolean;

    // stats
    maxHp:               number;
    hp:                  number;        
    attackSpeed:         number;
    range:               number;

    constructor(data: creatureConstructor){
        this.id         = data.id;
        this.name       = data.name;
        this.x          = data.x;
        this.y          = data.y;
        this.z          = data.z;
        this.maxHp      = data.maxHp;
        this.hp         = data.hp;
        this.direction  = data.direction || "S"
        this.sizeW      = this.sizeW  || 1;
        this.sizeH      = this.sizeH  || 1;
        this.speed      = data.speed  || 2;        
        this.sprint     = false;
        this.outfit     = data.outfit || "default";
        this.isMoving   = data.isMoving || false;   
        this.dead       = data.dead || false;    
        
        this.attackSpeed = data.attackSpeed || 1;
        this.range       = data.range || 100;
    }


    //  MOVEMENT ACTIONS
    setDirection(N: Boolean, E: Boolean, S:Boolean, W: Boolean){
                            
        if(N) this.direction = "N";
        if(E) this.direction = "E";
        if(S) this.direction = "S";
        if(W) this.direction = "W";
        
        if(N && E) this.direction = "NE";
        if(S && E) this.direction = "SE";
        if(S && W) this.direction = "SW";
        if(N && W) this.direction = "NW";
    }

    move(N: Boolean, E: Boolean, S:Boolean, W: Boolean) : void{                                           
        this.movementInProgress = false; // stop any movement in progress

        this.isMoving = true;        
        
        if(this.sprint) this.speedModifier = 1;        
        else this.speedModifier = 0;
                
        if(N)  this.y  -= this.speed + this.speedModifier;
        if(E)  this.x  += this.speed + this.speedModifier;
        if(S)  this.y  += this.speed + this.speedModifier;
        if(W)  this.x  -= this.speed + this.speedModifier;
        
        this.setDirection(N,E,S,W);
    }   

    stopMovement(): void{
        this.isMoving = false;
        this.movementInProgress = false;
    }

    moveToDirection(distance: number, direction: string){
        let walked = 0;
        let $this = this;
        this.isMoving = true;
        
        this.direction = direction;
        this.movementInProgress = true;
        
        let walking = setInterval(function(){            
            if(walked < distance || !$this.movementInProgress){
                walked += $this.speed;

                if(direction == "N")  $this.y  -= $this.speed;
                if(direction == "E")  $this.x  += $this.speed;
                if(direction == "S")  $this.y  += $this.speed;
                if(direction == "W")  $this.x  -= $this.speed;     
            } else {                
                clearInterval(walking);
                $this.stopMovement();
            }
        
        }, Config.interval);

    }

    moveTo(x: number, y:number){        
        let $this = this;

        if(this.movementInProgress){
            this.movementInProgress = false;      
            setTimeout(function(){
                $this.moveTo(x,y);
            }, 100)      
            return;
        }

        let startX = this.x;
        let startY = this.y;         
        let endX = false;
        let endY = false;
        
        this.isMoving = true;
        this.movementInProgress = true;
        
        
        let walking = setInterval(function(){                                        
            

            if($this.sprint) $this.speedModifier = 1;        
            else $this.speedModifier = 0;                        

            if(!$this.movementInProgress) stopProcessing();
            
            if(Math.abs(y - $this.y) >= $this.speed + 1){
                if(y < startY){
                    $this.direction = "N"; 
                    $this.y  -= $this.speed + $this.speedModifier;                    
                };
                if(y > startY){
                    $this.direction = "S"; 
                    $this.y  += $this.speed + $this.speedModifier;                    
                };
            } else endY = true;
             
            if(Math.abs(x - $this.x) >= $this.speed + 1){                
                
                if(x > startX){
                    $this.direction = "E"; 
                    $this.x  += $this.speed + $this.speedModifier;                    
                };                
                if(x < startX){
                    $this.direction = "W"; 
                    $this.x  -= $this.speed + $this.speedModifier;                    
                };

            } else endX = true;

            if(endX && endY) stopProcessing();                     
        
        }, Config.interval);


        const stopProcessing = () => {
            clearInterval(walking);
            this.stopMovement();
        }
    }

    // moveUp()

    startFollow(creature: Creature){        
        this.isFollowing = true;
        let following = setInterval(()=>{
            if(this.isFollowing && !creature.dead){
                let x = creature.x;
                let y = creature.y;
                let cDir = creature.direction;

                if(cDir == "N") y -= Config.scaled_size;
                if(cDir == "E") x += Config.scaled_size;
                if(cDir == "S") y += Config.scaled_size;
                if(cDir == "W") x -= Config.scaled_size;

                this.moveTo(x, y);
            } else {                                
                clearInterval(following);
                this.stopFollow(); 
            }
        }, 1500);
    }

    stopFollow(){        
        this.isFollowing = false;
    }
    

    // COMBAT ACTIONS
    attackCreature(creature: Creature) : void{                
        this.target = creature.id;
        this.attacking = creature.id;
        this.attack();
    }

    targetCreature(cid: string) : void{
        this.target = cid;
        this.attacking = null;        
    }

    removeTarget():void {
        this.target = null;
        this.attacking = null;        
        this.attackInProggress = false;
    }

    canAttack(creature): boolean {
        let res = true;                
        if(
            this.z != creature.z ||
            this.dead ||
            this.recentDeath||
            this.target != creature.id ||
            getDistance(this, creature) > this.range ||
            creature.dead
        ) res = false;

        return res;
    }

    attack() : void{                        
        let creatures = [...State.players, ...State.mobs];
        let cid = this.attacking;
        let creature = Object.values(creatures).find(c => c.id == cid);        
        
        if(!this.attackInProggress){
            this.attackInProggress = true;
            let processAttack = setInterval(()=>{            
                if(this.attacking){             
                       
                   this.processAttack(creature);
    
                } else {                                                                
                    clearInterval(processAttack);
                    this.attackInProggress = false;
                }
            }, 1500 / this.attackSpeed)
        }
    }

    processAttack(creature:Creature){
        if(creature.dead || creature.recentDeath) this.removeTarget();
        if(this.canAttack(creature)){                                                                                                   
            creature.takeDamage(30, this);
        }
    }

    takeDamage(damage: number, attacker?: Creature) : void {        
        if(damage >= this.hp){ 
            this.hp = 0;            
            this.processDeath();          
        }
        else this.hp -= damage;
    }    

    processDeath() : void{     
        this.dead = true;  
        this.recentDeath = true;         
           
        setTimeout(()=>{
            this.recentDeath = false;
        }, 5000);   
    };
    
}

export default Creature;