import Creature from "./Creature";
import {MobConstructor} from "../../../../shared/interfaces";
import State from "../../../state";
import { getDistance, getDistanceFromPoint} from "../utils";

class Mob extends Creature{       
    aggressive: boolean;
    radius:     number;    
    spawnX:     number;
    spawnY:     number;
    spawnZ:     number;

    constructor(data: MobConstructor){
        data.x = data.spawnX;
        data.y = data.spawnY;
        data.z = data.spawnZ;
        
        super(data);

        this.aggressive = data.aggressive;
        this.radius = data.radius;
        this.spawnX = data.spawnX;
        this.spawnY = data.spawnY;
        this.spawnZ = data.spawnZ;
        
        let $this = this;
        setInterval(function() : void{
           $this.patrol();
           if($this.aggressive) $this.watch();
        },500);
    }


    watch(){        
        
        if(!this.target && !this.attacking){            
            let target = Object.values(State.players).find(p => !p.dead && !p.recentDeath && p.z == this.z  && getDistance(this, p) <= 300);
            if(target){
                // if(target.length > 0) target = target[0];
                this.attackCreature(target);
            }
                    
        }
    }


    patrol(){        
        
        if(!this.isMoving && !this.isFollowing){
            let shouldMove = Math.floor(Math.random() * 20)  <= 2;                                 
            
            if(shouldMove){
                let directions = ["N","E","S","W"];        
                let direction = directions[Math.floor(Math.random() * 4)];                                 
                let distance = Math.floor(Math.random() * 150);
                let limitReached = false;

                if( (direction == "N") && this.y - distance < this.spawnY - this.radius ) limitReached = true;
                if( (direction == "E") && this.x + distance > this.spawnX + this.radius ) limitReached = true;
                if( (direction == "S") && this.y + distance > this.spawnY + this.radius ) limitReached = true;
                if( (direction == "W") && this.x - distance < this.spawnX - this.radius ) limitReached = true;

                if(!limitReached) this.moveToDirection(distance, direction);

            }
        }        
    }  
    

    startFollow(creature: Creature){  
        super.startFollow(creature);        
    }

    stopFollow(){
        super.stopFollow();
        this.moveTo(this.spawnX, this.spawnY);
    }


    attackCreature(creature: Creature) : void{
        super.attackCreature(creature);

        if(!this.isFollowing){
            this.sprint = true;
            this.startFollow(creature);
        }

        let distanceFromSpawn = setInterval(() => {
            if(getDistanceFromPoint(this, this.spawnX, this.spawnY) > 1000){
                clearInterval(distanceFromSpawn);                
                this.removeTarget();
                this.stopFollow();
            }
        }, 500);
    }

    processAttack(creature: Creature){
        super.processAttack(creature);        
    }

    takeDamage(damage: number, attacker?: Creature){
        super.takeDamage(damage, attacker);
        
        !this.attackInProggress && this.attackCreature(attacker);
    }

    processDeath() : void{     
        super.processDeath();     
        this.stopFollow();

        State.mobs = State.mobs.filter(m => m.id != this.id);        
        
        this.respawn();
    }

    respawn():void{
        setTimeout(() => {
            this.hp   = this.maxHp;
            this.x    = this.spawnX;
            this.y    = this.spawnY;
            this.z    = this.spawnZ;
            this.dead = false;

            State.mobs.push(this);    
        },5000);
    };

}

export default Mob;