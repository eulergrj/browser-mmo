import State from "../../state"
import Mob from "./classes/Mob"

export const loadMobs = () => {
    const mobs = require("../../data/mobs.json");
    
    mobs.forEach(m => {        
        let mob = new Mob(m); 
        State.mobs.push(mob);
        
    });
        
}