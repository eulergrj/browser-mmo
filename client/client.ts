import Config from "../shared/config"
import {initializeMap, drawMap, viewport, drawTopLayer, drawFringeLayers} from "./src/js/map"
import {initializeSpritesheets} from "./src/js/spritesheets"
import loadController from "./src/js/controllers"
import Utils from "./src/js/utils"
import State from "./src/js/clientState"
import Player from "./src/js/classes/Player"
import Mob from "./src/js/classes/Mob"

const socket = require('./socket.js');


const canvas = <HTMLCanvasElement>document.getElementById('game');
const ctx = canvas.getContext("2d");

State.ctx = ctx;

let started = false;

initializeSpritesheets();
initializeMap();

socket.on("init", (data) => {                                      
    if(data.selfId) State.selfId = data.selfId;
    data.players.forEach(p => {
        State.creatures[p.sid] = new Player(p);
        State.players[p.sid] = State.creatures[p.sid];
    });                    
    
    loadController(socket);
    
    if(!started) {
        gameLoop();
    }
});

socket.on("update", data => {      
    let mobs = data.mobs;
    
    const mdiff =  Object.values(State.creatures).filter((p1) => !mobs.some((p2) => p1.id === p2.id));    
    if(mdiff.length > 0) mdiff.forEach(p => delete State.creatures[p.id]);  
    mobs.forEach(m => {
        if(State.creatures[m.id]){
            State.creatures[m.id].update(m);
            State.mobs[m.id].update(m);
        } else {
            State.creatures[m.id] = new Mob(m);
            State.mobs[m.id] = State.creatures[m.id];
        }
    });



    let players = data.players;
    
    const diff =  Object.values(State.creatures).filter((p1) => !players.some((p2) => p1.sid === p2.sid));    
    if(diff.length > 0) diff.forEach(p => delete State.creatures[p.sid]);    

    players.forEach(p => {        
        if(State.creatures[p.sid]){
            State.creatures[p.sid].update(p);
            State.players[p.sid].update(p);
        } else {
            State.creatures[p.sid] = new Player(p);
            State.players[p.sid] = State.creatures[p.sid];
        }
    });      
    
    State.target = State.creatures[State.selfId].target;     
    State.attacking = State.creatures[State.selfId].attacking; 
    

});

socket.on("remoteSocketDisconnect", (sid:string) => {            
    delete State.creatures[sid];    
});

socket.on("serverMessage", (msg:string) => {            
    State.serverMessage = msg;

    setTimeout(()=>{
        State.serverMessage = null
    }, 2000);

    
});

function gameLoop(){    
    started = true;
    
    if(ctx==null) { return; }       
    
    let width  = document.documentElement.clientWidth - 10;
    let height = document.documentElement.clientHeight - 10;
    
    // Clear Canvas before drawing
    ctx.clearRect(0,0,width,height);

    /* Resize canvas on every frame */    
    canvas.width  = width;
    canvas.height = height;
    ctx.imageSmoothingEnabled = false;// prevent antialiasing of drawn image

    if(State.movement.isMoving) {socket.emit("move", State.movement)};    

    viewport.screen = [width,height];
    let self = State.creatures[State.selfId]; 
    viewport.update(
        self.x + (Config.tile_size / 2),
        self.y + (Config.tile_size / 2),   
        self.z
    );
            
    drawMap();       


    // DRAW CREATURES
    let creatures = Object.values(State.creatures);

    creatures.sort((c1,c2) => c1.y > c2.y && c1.z == c2.z ? 1 : -1);        
        
    Object.keys(creatures).forEach(key => {      
        let creature = creatures[key];                
        creature.draw();        
    });     

    drawFringeLayers(); 
    drawTopLayer();    
    
    
    // TEXTING LIGHT AND WEATHER
    // ctx.fillStyle = "rgba(169, 174, 178, .5)";
    // ctx.fillStyle = "rgba(237, 201, 175, .1)";
    // ctx.fillStyle = "rgba(0, 0, 0, .7)";
    // ctx.fillRect(0,0, viewport.screen[0], viewport.screen[1])



    let fps = Utils.calculateFps();    

    // DRAW FPS
    ctx.font = "bold 10pt sans-serif";	
    ctx.fillStyle = "#ffffff";
    ctx.strokeStyle = '#333';
    ctx.lineWidth = 3;
    ctx.textAlign = "left";
    ctx.strokeText("FPS: " + fps, 10, 20);        
    ctx.fillText("FPS: " + fps, 10, 20);    

    // Draw Coordinates
    ctx.font = "bold 10pt sans-serif";	
    ctx.fillStyle = "#ffffff";
    ctx.strokeStyle = '#333';
    ctx.lineWidth = 3;
    ctx.textAlign = "left";    
    ctx.strokeText("" + (State.creatures[State.selfId].x/Config.tile_size).toFixed(3) + ' , ' + (State.creatures[State.selfId].y/Config.tile_size).toFixed(3), width - 100, 20);    
    ctx.fillText("" + (State.creatures[State.selfId].x/Config.tile_size).toFixed(3) + ' , ' + (State.creatures[State.selfId].y/Config.tile_size).toFixed(3), width - 100, 20);    
    
    

    // Draw Server Messages
    if(State.serverMessage){
        ctx.font = "bold 12pt sans-serif";	
        ctx.fillStyle = "#ffffff";
        ctx.strokeStyle = '#333';
        ctx.lineWidth = 3;
        ctx.textAlign = "center";    
        ctx.strokeText(State.serverMessage, document.documentElement.clientWidth/2, document.documentElement.clientHeight - 20);    
        ctx.fillText(State.serverMessage, document.documentElement.clientWidth/2, document.documentElement.clientHeight - 20);   
    }
        


    requestAnimationFrame(gameLoop);
}














