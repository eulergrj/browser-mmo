const socket = io();
let socketid;
socket.on('connect', () =>  socketid = socket.io.engine.id);

module.exports = socket;