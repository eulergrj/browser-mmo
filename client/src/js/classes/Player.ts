import Creature from "./Creature";
import {playerConstructor} from "../../../../shared/interfaces";

class Player extends Creature{       
    sid: string;

    constructor(data: playerConstructor){
        super(data);
        this.sid = data.sid;
    }    
    

}


export default Player;