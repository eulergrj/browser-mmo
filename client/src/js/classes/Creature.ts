import Config from "../../../../shared/config"
import {viewport} from "../map"
import {spritesheets} from "../spritesheets"
import {creatureConstructor} from "../../../../shared/interfaces";
import State from "../clientState"
import { Context } from "vm";

let tile_size = Config.tile_size;
let scaled_size = Config.scaled_size;


let outfits = {
    "default": {
        gender: 'male',
        width: 32,
        height: 32,
        animations: {
            walk: {
                N:  {length: 3, col: 0},
                NE: {length: 3, col: 1},
                E:  {length: 3, col: 1},
                SE: {length: 3, col: 1},
                S:  {length: 3, col: 2},
                SW: {length: 3, col: 3},
                W:  {length: 3, col: 3},
                NW: {length: 3, col: 3},
                
            },
            idle: {
                N:  {length: 1, col: 0},
                NE: {length: 1, col: 1},
                E:  {length: 1, col: 1},
                SE: {length: 1, col: 1},
                S:  {length: 1, col: 2},
                SW: {length: 1, col: 3},
                W:  {length: 1, col: 3},
                NW: {length: 1, col: 3},

            }
        },
        offsetX: 0,
        offsetY: 0
    },
    "robe": {
        gender: 'male',
        width: 32,
        height: 32,
        animations: {
            walk: {
                N:  {length: 3, col: 4},
                NE: {length: 3, col: 5},
                E:  {length: 3, col: 5},
                SE: {length: 3, col: 5},
                S:  {length: 3, col: 6},
                SW: {length: 3, col: 7},
                W:  {length: 3, col: 7},
                NW: {length: 3, col: 7},
                
            },
            idle: {
                N:  {length: 1, col: 4},
                NE: {length: 1, col: 5},
                E:  {length: 1, col: 5},
                SE: {length: 1, col: 5},
                S:  {length: 1, col: 6},
                SW: {length: 1, col: 7},
                W:  {length: 1, col: 7},
                NW: {length: 1, col: 7},

            }
        },
        offsetX: 0,
        offsetY: 0
    },
    "cultist": {
        gender: 'male',
        width: 32,
        height: 32,
        animations: {
            walk: {
                N:  {length: 3, col: 0},
                NE: {length: 3, col: 1},
                E:  {length: 3, col: 1},
                SE: {length: 3, col: 1},
                S:  {length: 3, col: 2},
                SW: {length: 3, col: 3},
                W:  {length: 3, col: 3},
                NW: {length: 3, col: 3},
                
            },
            idle: {
                N:  {length: 1, col: 0},
                NE: {length: 1, col: 1},
                E:  {length: 1, col: 1},
                SE: {length: 1, col: 1},
                S:  {length: 1, col: 2},
                SW: {length: 1, col: 3},
                W:  {length: 1, col: 3},
                NW: {length: 1, col: 3},

            }
        },
        offsetX: 0,
        offsetY: 3
    },
    
}

class Creature{   
    id:         string;    
    name:       string;        
    x:          number;
    y:          number;
    z:          number;
    maxHp:      number;
    hp:         number;        
    isMoving:   boolean;
    direction:  string;
    sizeW:      number;
    sizeH:      number;
    outfit?:    string;
    speed:      number;
    frame:      number;
    target?:    string;
    attacking?: string;  
    dead:       boolean;  

    constructor(data: creatureConstructor){
        this.id         = data.id;
        this.name       = data.name;
        this.x          = data.x;
        this.y          = data.y;
        this.z          = data.z;
        this.maxHp      = data.maxHp;
        this.hp         = data.hp;
        this.direction  = data.direction || "down"
        this.sizeW      = this.sizeW  || 1;
        this.sizeH      = this.sizeH  || 1;
        this.speed      = data.speed  || 1;
        this.isMoving   = data.isMoving || false;
        this.outfit     = data.outfit || "default";           
        this.dead       = data.dead || false;           
        
    }


    update(data: creatureConstructor): void{                
        this.x          = data.x;
        this.y          = data.y;
        this.z          = data.z;
        this.maxHp      = data.maxHp;
        this.hp         = data.hp;
        this.direction  = data.direction || "S"
        this.sizeW      = this.sizeW  || 1;
        this.sizeH      = this.sizeH  || 1;
        this.speed      = data.speed  || 1;
        this.isMoving   = data.isMoving || false;
        this.outfit     = data.outfit || "default";
        this.target     = data.target || null;
        this.attacking  = data.attacking || null;
        this.dead       = data.dead;
    }
    
    draw(): void{
        this.drawName();
        this.drawHpBar();
        this.drawOutfit();
    }


    private getHpColor(): string{
        let color = "#57ac5b"
        let percent = (100 * this.hp) / this.maxHp;
        
        if(percent <= 90) color = "#4d9850";
        if(percent <= 80) color = "#77ac57";
        if(percent <= 70) color = "#83ac57";
        if(percent <= 60) color = "#8fac57";
        if(percent <= 50) color = "#9dac57";
        if(percent <= 40) color = "#acab57";      
        if(percent <= 30) color = "#ac9d57";   
        if(percent <= 20) color = "#8f4141"; 
        if(percent <= 10) color = "#6c3030";                   

        return color;
    }


    private drawName(): void {
        let ctx = State.ctx;
        ctx.fillStyle = this.getHpColor();
        ctx.lineWidth = 3;
        ctx.strokeStyle = '#222';
        ctx.textAlign = "center";
        ctx.font = "bold 13px 'Mukta'";                     

        let xpos = viewport.offset[0] + (this.x - scaled_size*Config.creatureOffset) + 20;
        let ypos = viewport.offset[1] + (this.y - scaled_size*Config.creatureOffset) - 22;

        ctx.strokeText(this.name, xpos, ypos); 
        ctx.fillText(this.name,xpos, ypos);   
                
    }

    private drawHpBar(): void {      
        let ctx = State.ctx;
        let percent = ((100 * this.hp) / this.maxHp) / 100;

        ctx.fillStyle = "#222";
        ctx.fillRect(
            viewport.offset[0] + (this.x - scaled_size*Config.creatureOffset) - 1,
            viewport.offset[1] + (this.y - scaled_size*Config.creatureOffset) - 16,
            42, 5
        );

        ctx.fillStyle = this.getHpColor();        
        ctx.fillRect(
            viewport.offset[0] + (this.x - scaled_size*Config.creatureOffset),
            viewport.offset[1] + (this.y - scaled_size*Config.creatureOffset) - 15,
            40 * percent, 3
        );
        
                
    }

    private drawOutfit(): void{
        let ctx = State.ctx;
        let outfit = outfits[this.outfit];  
        
        if(!spritesheets["outfits"]) return;        
        let animationType = this.isMoving ? "walk" : "idle";
        let animation = outfit.animations[animationType][this.direction];    
        
        let imagePosX = animation.col;
        let imagePosY = (Math.floor(this.frame) % animation.length) + outfit.offsetY;
        
        if(Config.debug){
            ctx.fillStyle = "red";
            ctx.fillRect(viewport.offset[0] + (this.x - scaled_size*Config.creatureOffset), viewport.offset[1] + (this.y - scaled_size*Config.creatureOffset),scaled_size, scaled_size);
        }

        if(this.id == State.target){       
            ctx.beginPath();
            ctx.arc(viewport.offset[0] + this.x, viewport.offset[1] + this.y, scaled_size*.5, 0, 2 * Math.PI, false);
            ctx.fillStyle = !!State.attacking ? 'rgba(255,0,0,.3)' : 'rgba(255,255,255,.3)';
            ctx.fill();
            ctx.lineWidth = 2;
            ctx.strokeStyle = !!State.attacking ? 'rgba(255,0,0,.5)' : 'rgba(255,255,255,.5)';
            ctx.stroke();     
        }

        ctx.drawImage(
            spritesheets["outfits"], 
            imagePosX * tile_size, imagePosY * tile_size, 
            tile_size, tile_size, 
            viewport.offset[0] + (this.x - scaled_size*Config.creatureOffset), viewport.offset[1] + (this.y - scaled_size*Config.creatureOffset),
            this.sizeW * scaled_size, this.sizeH * scaled_size
        );                
        
            
        if(this.isMoving) this.frame += 0.14;        
        else this.frame = outfit.offsetY;
    }
}

export default Creature;