import Creature from "./Creature";
import {MobConstructor} from "../../../../shared/interfaces";

class Mob extends Creature{       
    aggressive: boolean;
    radius:     number;

    constructor(data: MobConstructor){
        super(data);
        this.aggressive = data.aggressive;
        this.radius = data.radius;
    }

}

export default Mob;