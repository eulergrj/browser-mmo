const State = {
    selfId: null,
    canvas: null,
    ctx: null,
    cursorX: 0,
    cursorY: 0,
    movement: {N: false, E: false, S: false, W: false, sprint: false, isMoving: false},    
    players: [],    
    mobs: [],
    creatures: [],
    target: null,
    attacking: null,
    serverMessage: null,
    spritesheetsToLoad: 1,    
}

export default State;