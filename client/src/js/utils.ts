import Config from "../../../shared/config"
import State from "../js/clientState"

let currentSecond = 0;
let frameCount = 0;
let framesLastSecond=0;

const Utils = {
  xy2i: (x,y,w) => {
    return y * w + x;
  },  

  i2xy: (i,w) =>{
      return [i%w, Math.floor(i/w)];
  },

  calculateFps: () => {
    let sec = Math.floor(Date.now()/1000);
    if(sec!=currentSecond){
      currentSecond = sec;
      framesLastSecond = frameCount;
      frameCount = 1;
    } else { 
      frameCount++; 
    }
    return framesLastSecond;
  },
  
}

export const pixelToVectorPosition = (pixel: number) => Math.floor(pixel / Config.scaled_size);


export const creatureOnPosition = (cursorX: number, cursorY:number, cursorZ: number) => {
    let res = null;
    let creatures = Object.values(State.creatures);    
    let scaled_size = Config.scaled_size;
    let creatureOffeset = Config.creatureOffset;
    
    let creature = creatures.filter(p => {
      let minX = p.x - scaled_size*creatureOffeset;
      let maxX = minX + scaled_size + 10;
      let minY = p.y - scaled_size*creatureOffeset;
      let maxY = minY + scaled_size + 10;      

      return (cursorX >= minX && cursorX <= maxX) && (cursorY >= minY && cursorY <= maxY) && (cursorZ == p.z);
      
    }); 

    if(creature[0]){
      res = creature[0].id;
    }

    return res;
}


export default Utils;