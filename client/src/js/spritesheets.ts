import State from "./clientState"

export const spritesheets = [];

export const initializeSpritesheets = () => {
    let list = [        
        {name:"tiles", src: "../../assets/spritesheets/32/tiles.png"},
        {name:"outfits", src: "../../assets/spritesheets/32/outfits.png"},             
        {name:"buildings", src: "../../assets/spritesheets/64/buildings.png"},        
        {name:"nature", src: "../../assets/spritesheets/64/nature.png"},        
    ];

    State.spritesheetsToLoad = list.length;

    list.forEach(i => {        
        let img = new Image();

        img.addEventListener("load", (event) => { 
            State.spritesheetsToLoad--;
            spritesheets[i.name.toString()] = img;
        });

        img.src = i.src;	
    })
}