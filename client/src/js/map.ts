import Config from "../../../shared/config"
import {spritesheets} from "../../src/js/spritesheets"
import Utils from "./utils"
import $ from "jquery"
import { Context } from "vm";
import State from "./clientState";
import { getDistance, getDistanceFromPoint } from "../../../server/src/js/utils";

let map;
let tile_size = Config.tile_size;
let scaled_size = Config.scaled_size;
let mapLoaded = false;
let fringeLayerArr = [];


export const initializeMap = () => {    
    $.get("../../assets/maps/map.json", function(data){
        map = data;                
        mapLoaded = true;
    });
}

export const viewport = {
	screen		: [0,0],
	startTile	: [0,0],
	endTile		: [0,0],
	offset		: [0,0,0],
	update		: function(px, py, pz) {    
        
        if(!mapLoaded) return;        

        this.offset[2] = pz;
        
        this.offset[0] = Math.floor((this.screen[0]/2) - px);
        this.offset[1] = Math.floor((this.screen[1]/2) - py); 


        // Tying to avoid black side when approaching map end
        // if(Math.floor((this.screen[0]/2) - px) <= -tile_size * .5 + 1) this.offset[0] = Math.floor((this.screen[0]/2) - px);
        // if(Math.floor((this.screen[1]/2) - py) <= -tile_size * .5 + 1) this.offset[1] = Math.floor((this.screen[1]/2) - py);                  
        
        // if(Math.floor((this.screen[0]/2) - px) <= (-map.width * tile_size) - px) this.offset[0] = -map.width * tile_size;
        // if(Math.floor((this.screen[1]/2) - py) <= (-map.height * tile_size) - py) this.offset[1] = -map.height * tile_size;

		var tile = [ Math.floor(px/scaled_size), Math.floor(py/scaled_size) ];

		this.startTile[0] = tile[0] - 1 - Math.ceil((this.screen[0]/2) / scaled_size);
		this.startTile[1] = tile[1] - 1 - Math.ceil((this.screen[1]/2) / scaled_size);

		if(this.startTile[0] < 0) { this.startTile[0] = 0; }
		if(this.startTile[1] < 0) { this.startTile[1] = 0; }

		this.endTile[0] = tile[0] + 1 + Math.ceil((this.screen[0]/2) / scaled_size);
		this.endTile[1] = tile[1] + 1 + Math.ceil((this.screen[1]/2) / scaled_size);

		if(this.endTile[0] >= map.width) { this.endTile[0] = map.width - 1; }
		if(this.endTile[1] >= map.height) { this.endTile[1] = map.height - 1; }
	}
};


export function drawMap(){                  
    if(map == null || State.spritesheetsToLoad > 0){ return};
    

    let layers = map.layers;    
    
    let pz = viewport.offset[2];

    // DRAW LAYER BELOW PLAYER
    // let layerBelow = layers.find(l => parseInt(l.name) == pz - 1);      
    // layerBelow && layerBelow.layers.forEach((layer) => {                
    //     drawLayer(layer);
    // }); 

    // DRAW PLAYER LAYER
    let layerAt = layers.find(l => parseInt(l.name) == pz);      
    layerAt && layerAt.layers.forEach((layer) => {  
        let fringeLayer = layer.name == "Fringe";
    
        if(!fringeLayer){              
            drawLayer(layer);
        } else {
            let find = fringeLayerArr.find(l => l.id == layer.id);
            if(!find){
                fringeLayerArr.unshift(layer);
            }
        }        
        
    });   
  
}

function drawLayer(layer){        
    if(!layer.visible) return;     
    
    let ctx = State.ctx;
    
    for (let x = viewport.startTile[0]; x < viewport.endTile[0]; x++) {
        for(let y = viewport.startTile[1]; y < viewport.endTile[1]; y++) {                        

            let val = layer.data[Utils.xy2i(x,y, layer.width)];

            if(val > 0){
                let tileset = map.tilesets.find(ts => val >= ts.firstgid && val <= ts.firstgid + ts.tilecount);           
    
                val -= tileset.firstgid;            
                
                let spritesheet = spritesheets[tileset.name];
    
                let imgPos = Utils.i2xy(val, (spritesheet.width / tileset.tilewidth));    
                let posX = viewport.offset[0] + x * scaled_size;
                let posY = tileset.tileheight> tile_size ? viewport.offset[1] + y * scaled_size - scaled_size : viewport.offset[1] + y * scaled_size;    
                
                let item = {
                    order: 0,
                    spritesheet: spritesheet,
                    imgPosX: imgPos[0]*tileset.tilewidth, 
                    imgPosY: imgPos[1]*tileset.tileheight,
                    imgSizeX: tileset.tilewidth, 
                    imgSizeY: tileset.tileheight,
                    posX: posX, 
                    posY: posY,
                    sizeX: tileset.tilewidth * Config.scale,
                    sizeY:  tileset.tileheight * Config.scale
                }
                
                ctx.drawImage(  
                    spritesheet, // image sheet
                    imgPos[0]*tileset.tilewidth, imgPos[1]*tileset.tileheight, //x and y pos of image on sheet
                    tileset.tilewidth, tileset.tileheight, // size of image on sprite sheet                
                    posX, posY, // x and y of pos of where to draw
                    tileset.tilewidth * Config.scale,  tileset.tileheight * Config.scale // size of the image to be drawn
                );
            }                                    

                        
        }
    }    
    
}





export const drawTopLayer = () => {
    if(map == null || State.spritesheetsToLoad > 0) return;

    let ctx = State.ctx;

    let layers = map.layers;    
    
    
    let pz = viewport.offset[2];

    // DRAW LAYER ABOVE PLAYER
    let layersAbove = layers.filter(l => parseInt(l.name) > pz);
    
    let hide = false;
    layersAbove && layersAbove.forEach(layerAbove => {
        layerAbove.layers.forEach((layer) => {                

            let player = State.creatures[State.selfId];
    
            let aboveTile = layer.data.filter((d, i) => {
                if(d > 0){                
                    let pos = Utils.i2xy(i, layer.width);
                    let x = pos[0] * scaled_size + 60;
                    let y =  pos[1] * scaled_size + 60;
    
                    return getDistanceFromPoint(player, x, y) < scaled_size*Config.creatureOffset;                        
                }            
            });                        
    
            if(aboveTile.length > 0) hide = true;
            if(!hide) drawLayer(layer);
            // else fringeLayerArr = fringeLayerArr.filter(l => l.id == layer.id);
        });  
    });            
    
}


export const drawFringeLayers = () => {
    let layers = fringeLayerArr;
    
    layers.forEach((layer) => {        
        drawLayer(layer);
    });
        
}