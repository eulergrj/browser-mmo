import Config from "../../../shared/config"
import {spritesheets} from "../../src/js/spritesheets"
import Utils from "./utils"
import $ from "jquery"
import { Context } from "vm";
import State from "./clientState";

let map;
let tile_size = Config.tile_size;
let scaled_size = Config.scaled_size;
let mapLoaded = false;


export const initializeMap = () => {    
    $.get("../../assets/maps/map.json", function(data){
        map = data;                
        mapLoaded = true;
    });
}

export const viewport = {
	screen		: [0,0],
	startTile	: [0,0],
	endTile		: [0,0],
	offset		: [0,0,0],
	update		: function(px, py, pz) {    
        
        if(!mapLoaded) return;        

        this.offset[2] = pz;
        
        this.offset[0] = Math.floor((this.screen[0]/2) - px);
        this.offset[1] = Math.floor((this.screen[1]/2) - py); 


        // Tying to avoid black side when approaching map end
        // if(Math.floor((this.screen[0]/2) - px) <= -tile_size * .5 + 1) this.offset[0] = Math.floor((this.screen[0]/2) - px);
        // if(Math.floor((this.screen[1]/2) - py) <= -tile_size * .5 + 1) this.offset[1] = Math.floor((this.screen[1]/2) - py);                  
        
        // if(Math.floor((this.screen[0]/2) - px) <= (-map.width * tile_size) - px) this.offset[0] = -map.width * tile_size;
        // if(Math.floor((this.screen[1]/2) - py) <= (-map.height * tile_size) - py) this.offset[1] = -map.height * tile_size;

		var tile = [ Math.floor(px/scaled_size), Math.floor(py/scaled_size) ];

		this.startTile[0] = tile[0] - 1 - Math.ceil((this.screen[0]/2) / scaled_size);
		this.startTile[1] = tile[1] - 1 - Math.ceil((this.screen[1]/2) / scaled_size);

		if(this.startTile[0] < 0) { this.startTile[0] = 0; }
		if(this.startTile[1] < 0) { this.startTile[1] = 0; }

		this.endTile[0] = tile[0] + 1 + Math.ceil((this.screen[0]/2) / scaled_size);
		this.endTile[1] = tile[1] + 1 + Math.ceil((this.screen[1]/2) / scaled_size);

		if(this.endTile[0] >= map.width) { this.endTile[0] = map.width - 1; }
		if(this.endTile[1] >= map.height) { this.endTile[1] = map.height - 1; }
	}
};


export function drawMap(ctx){         
    let spritesheet = spritesheets["world"];
    
    if(map == null || !spritesheet || !ctx){return}          

    let layers = map.layers;    
    
    let pz = viewport.offset[2];

    // DRAW LAYER BELOW PLAYER
    // let layerBelow = layers.find(l => parseInt(l.name) == pz - 1);      
    // layerBelow && layerBelow.layers.forEach((layer) => {                
    //     drawLayer(ctx, spritesheet, layer);
    // }); 

    // DRAW PLAYER LAYER
    let layerAt = layers.find(l => parseInt(l.name) == pz);      
    layerAt && layerAt.layers.forEach((layer) => {                
        drawLayer(ctx, spritesheet, layer);
    });   

    
     // DRAW LAYER ABOVE PLAYER
    //  let layerAbove = layers.find(l => parseInt(l.name) == pz + 1);      
    //  layerAbove && layerAbove.layers.forEach((layer) => {                
    //      drawLayer(ctx, spritesheet, layer);
    //  });    
}

function drawLayer(ctx, spritesheet, layer){        
    if(!layer.visible) return;       

    let chunks = layer.chunks.filter(c => 
        c.x >= viewport.startTile[0] - c.width && c.x <= viewport.endTile[0] &&
        c.y >= viewport.startTile[1] - c.height && c.y <= viewport.endTile[1] 
    );    
    
    chunks.forEach(c => {                        
        for (let x = c.x; x < c.x + c.width; x++) {
            for(let y = c.y; y < c.y + c.height; y++) {                        
        
                let val = c.data[Utils.xy2i(x,y, c.width)] - 1;                                
                // console.log(val);
                let imgPos = Utils.i2xy(val, (spritesheet.width / tile_size));    
                
                // ctx.fillStyle = "red";
                // ctx.fillRect(
                //     viewport.offset[0] + x*scaled_size, viewport.offset[1] + y*scaled_size,
                //     scaled_size, scaled_size
                // )
                ctx.drawImage(
                    spritesheet, // image sheet
                    imgPos[0]*tile_size, imgPos[1]*tile_size, //x and y pos of image on sheet
                    tile_size, tile_size, // size of image on sprite sheet
                    viewport.offset[0] + x*scaled_size, viewport.offset[1] + y*scaled_size, // x and y of pos of where to draw
                    scaled_size,  scaled_size // size of the image to be drawn
                );
            }
            }
    })
    

    // layer.chunks.forEach(c => {                        
    //     for (let x = viewport.startTile[0]; x < viewport.endTile[0]; x++) {
    //         for(let y = viewport.startTile[1]; y < viewport.endTile[1]; y++) {                        
        
    //             let val = c.data[Utils.xy2i(x,y, c.width)] - 1;
    
    //             let imgPos = Utils.i2xy(val, (spritesheet.width / tile_size));    
    
                
    //             ctx.drawImage(
    //                 spritesheet, // image sheet
    //                 imgPos[0]*tile_size, imgPos[1]*tile_size, //x and y pos of image on sheet
    //                 tile_size, tile_size, // size of image on sprite sheet
    //                 viewport.offset[0] + x*scaled_size, viewport.offset[1] + y*scaled_size, // x and y of pos of where to draw
    //                 scaled_size,  scaled_size // size of the image to be drawn
    //             );
    //         }
    //       }
    // })
}