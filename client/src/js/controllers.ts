import State from "./clientState"
import {viewport} from "../../src/js/map"
import Config from "../../../shared/config"
import {creatureOnPosition, pixelToVectorPosition} from "../js/utils"

let KEY_UP = 38;
let KEY_RIGHT = 39;
let KEY_DOWN = 40;
let KEY_LEFT = 37;

let KEY_SHIFT = 16;
let KEY_ESC = 27;

let KEY_W = 87;
let KEY_D = 68;
let KEY_S = 83;
let KEY_A = 65;

let R_MOUSE = 1;
let M_MOUSE = 2;
let L_MOUSE = 3;

let KEY_TAB = 9;

const loadController = (socket) => {
    document.addEventListener('keydown', (e) => {                            
        e.preventDefault();                            
        if(e.keyCode == KEY_UP    || e.keyCode == KEY_W)  State.movement.N = true;
        if(e.keyCode == KEY_RIGHT || e.keyCode == KEY_D)  State.movement.E = true;
        if(e.keyCode == KEY_DOWN  || e.keyCode == KEY_S)  State.movement.S = true;
        if(e.keyCode == KEY_LEFT  || e.keyCode == KEY_A)  State.movement.W = true;

        if(e.keyCode == KEY_SHIFT)  socket.emit("toggleSprint");
        if(e.keyCode == KEY_ESC)  socket.emit("escapeKey");

        if(State.movement.N || State.movement.E || State.movement.S || State.movement.W) {
            State.movement.isMoving = true;                        
        }
            
    });
    
    document.addEventListener('keyup', (e) => {                 
        e.preventDefault();
        if(e.keyCode == KEY_UP    || e.keyCode == KEY_W)  State.movement.N = false;
        if(e.keyCode == KEY_RIGHT || e.keyCode == KEY_D)  State.movement.E = false;
        if(e.keyCode == KEY_DOWN  || e.keyCode == KEY_S)  State.movement.S = false;
        if(e.keyCode == KEY_LEFT  || e.keyCode == KEY_A)  State.movement.W = false;        
        
        if(!State.movement.N && !State.movement.E && !State.movement.S && !State.movement.W) {            
            State.movement.isMoving = false;
            socket.emit("stopMovement");
        }
    });

    document.addEventListener('mousedown', (e) => {    
        e.preventDefault();        

        let x = e.pageX - viewport.offset[0];
        let y = e.pageY - viewport.offset[1];
        let z = State.creatures[State.selfId].z;        
        
        let creature = creatureOnPosition(x, y, z);            

        if(creature){
            socket.emit("targetCreature", creature, e.which == L_MOUSE);            
        } else {            
            if(e.which == R_MOUSE) socket.emit("moveTo", x,y);
        }
        
        
        
    });


    document.addEventListener('mousemove', (e) => {      
        let x = e.pageX - viewport.offset[0];
        let y = e.pageY - viewport.offset[1];
        let z = 0;        
                
        let creature = creatureOnPosition(x, y, z);        
        
        if(creature){
            document.body.style.cursor = "default";
        } else {
            document.body.style.cursor = "default";
        }

        // let vectorX = pixelToVectorPosition(x);
        // let vectorY = pixelToVectorPosition(y);
        

        
    });
    
    
    // prevent context menu
    if (document.addEventListener) {
        document.addEventListener('contextmenu', function (e) {
            e.preventDefault();
        }, false);
    } else {
        // document.attachEvent('oncontextmenu', function () {
        //     window.event.returnValue = false;
        // });
    }
}


export default loadController;