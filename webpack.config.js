const path = require('path');
const nodeExternals = require('webpack-node-externals');

const clientConfig = {
  mode: "development",
  entry: './client/client.ts',
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
    ],
  },
  resolve: {
    extensions: [ '.tsx', '.ts', '.js' ],
  },
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'client/build'),
  },
};

const serverConfig = {
  mode: "production",
  target: "node",
  entry: './server/server.ts',
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
    ],
  },
  resolve: {
    extensions: [ '.tsx', '.ts' ],
  },
  output: {
    filename: 'server.js',
    path: path.resolve(__dirname, 'server/build'),
  },
  node: {
    __dirname: false
  },
  externals: [ nodeExternals() ]
};

module.exports = [clientConfig, serverConfig];