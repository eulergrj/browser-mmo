export interface creatureConstructor{
    id:           string;
    name:         string;    
    maxHp:        number;
    hp:           number;        
    x:            number;
    y:            number;    
    z:            number;    
    outfit?:      string;
    isMoving?:    boolean;
    direction?:   string;
    sizeW?:       number;
    sizeH?:       number;
    speed?:       number;
    target?:      string;
    attacking?:   string;  
    dead:         boolean;    

    // stats
    attackSpeed?: number;
    range?:       number;
}


export interface playerConstructor extends creatureConstructor{
    sid: string;    
}

export interface MobConstructor extends creatureConstructor{
    radius: number;
    aggressive: boolean;
    spawnX: number;
    spawnY: number;
    spawnZ: number;
}

