const Config = {
    debug: false,
    tile_size: 32,
    scale: 2,
    scaled_size: 0,
    interval: 1000 / 60,    
    creatureOffset: .8
}
Config.scaled_size = Config.tile_size * Config.scale;

export default Config;